CREATE DATABASE task24;

USE task24;

-- CREATE TABLE AUTHOR
CREATE TABLE author (
	id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    first_name VARCHAR(128),
    last_name VARCHAR(128),
    created_at DATETIME DEFAULT NOW() NOT NULL,
    updated_at DATETIME DEFAULT NULL
);

-- JOURNAL
CREATE TABLE journal (
	id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    NAME VARCHAR(128) NOT NULL,
	created_at DATETIME DEFAULT NOW() NOT NULL,
    updated_at DATETIME DEFAULT NULL
);


CREATE TABLE journal_entry (
	id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    title VARCHAR(128) NOT NULL,
    `text` TEXT,
	created_at DATETIME DEFAULT NOW() NOT NULL,
    updated_at DATETIME DEFAULT NULL,
    author_id INT NOT NULL,
    journal_id INT NOT NULL,
    FOREIGN KEY (author_id) REFERENCES author(id),
    FOREIGN KEY (journal_id) REFERENCES journal(id)

);

CREATE TABLE `tags` (
	id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    title VARCHAR(128) NOT NULL,
    created_at DATETIME DEFAULT NOW() NOT NULL,
    updated_at DATETIME DEFAULT NULL
);



-- LINKING TABLE
CREATE TABLE journal_tag (
	id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    tag_id INT,
    journal_entry_id INT,
	FOREIGN KEY (tag_id) REFERENCES tag(id),
	FOREIGN KEY (journal_entry_id) REFERENCES journal_entry(id)
    );

INSERT INTO author (first_name, last_name)
VALUES ('J.K', 'Rowling'), ('Walter', 'Isacsson'), ('Malcolm', 'Gladwell');

INSERT INTO journal (name)
VALUES ('Fantasy'), ('Biography'), ('Psycology');

INSERT INTO journal_entry (title, text, author_id, journal_id)
VALUES ('Harry Potter and the Philosophers Stone',
'Harry Potter is a wizard, and he has a place at Hogwarts School of Witchcraft and Wizardry. An incredible adventure is about to begin!', 1, 1),

('Steve Jobs',
 'Drawn from three years of exclusive and unprecedented interviews Isaacson has conducted with Jobs as well as extensive interviews with Jobs family members, key colleagues from Apple and its competitors, Steve Jobs: The Exclusive Biography is the definitive portrait of the greatest innovator of his generation.', 2, 2),

('Blink',
 'Blink reveals that great decision makers arent those who process the most information or spend the most time deliberating, but those who have perfected the art of "thin-slicing", filtering the very few factors that matter from an overwhelming number of variables.', 3, 3);


INSERT INTO tag (title)
VALUES ('Book'), ('E-book'), ('Web-book');

ALTER TABLE journal_entry
ADD release_year INT;

UPDATE  journal_entry
SET release_year = 1997 where id =1;

UPDATE  journal_entry
SET release_year = 2011 where id =2;

UPDATE  journal_entry
SET release_year = 2005 where id =3;

SELECT * FROM journal_entry;

SELECT * FROM journal_entry JOIN author ON journal_entry.id = author.id;

SELECT DISTINCT title FROM Tag;

SELECT * FROM author WHERE id = 3;

INSERT INTO journal_entry (title, text, author_id, journal_id)
VALUES ('Talking to Strangers',
'Malcolm Gladwell, host of the podcast Revisionist History and author of the #1 New York Times bestseller Outliers, offers a powerful examination of our interactions with strangers -- and why they often go wrong.', 3, 3);

UPDATE  journal_entry
SET release_year = 2019 WHERE id =4;

SELECT * FROM journal_entry INNER JOIN author ON journal_entry.id = author.id;

SELECT * FROM journal_entry LEFT JOIN author ON journal_entry.id = author.id;

SELECT * FROM journal_entry RIGHT JOIN author ON journal_entry.id = author.id;

SELECT SUM(release_year) FROM journal_entry;

SELECT release_year, COUNT(release_year) FROM journal_entry;

SELECT release_year, COUNT(release_year) FROM journal_entry WHERE id =3;